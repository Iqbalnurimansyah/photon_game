﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using System;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PlayerController : MonoBehaviourPun, IPunObservable
{
    public PhotonView pv;
    public Rigidbody2D rb;
    public GameObject playerCamera, chatBubble;
    public TextMeshProUGUI username, chatText;

    private bool isCollided;
    public float jumpForce;
    public int moveSpeed;

    private Vector3 jump, smoothMove;
    private const byte CHAT_EVENT = 0;

    private void Start()
    {
        if (photonView.IsMine)
        {
            playerCamera.SetActive(true);
            username.text = PhotonNetwork.NickName;
            gameObject.tag = "myPlayer";
        }
        else
        {
            username.text = pv.Owner.NickName;
        }
        jump = new Vector3(0.0f, 5.0f, 0.0f);
        isCollided = false;
    }

    private void Update()
    {
        if (photonView.IsMine && !isCollided)
        {
            CheckInput();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                rb.AddForce(jump * jumpForce, ForceMode2D.Impulse);
            }
        }
        else
        {
            SmoothMovement();
        }
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
    }

    private void NetworkingClient_EventReceived(EventData obj)
    {
        if(obj.Code == CHAT_EVENT)
        {
            object[] datas = (object[]) obj.CustomData;
            string message = (string) datas[0];
            int viewId = (int) datas[1];
            if(photonView.ViewID == viewId)
            {
                chatBubble.SetActive(true);
                chatText.text = message;
                Invoke("ResetChat", 5f);
            }
        }
    }

    public void SetGroup(int group)
    {
        byte[] enabledGroups = { (byte)group };
        PhotonNetwork.SetInterestGroups(new byte[0], enabledGroups);
    }

    public bool GetIsMine()
    {
        return photonView.IsMine;
    }

    private void SmoothMovement()
    {
        transform.position = Vector3.Lerp(transform.position, smoothMove, Time.deltaTime * 7);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "platform")
        {
            isCollided = true;
            if (!GetComponent<SpriteRenderer>().flipX)
                transform.position -= new Vector3(2f, 0f, 0f);
            else
                transform.position += new Vector3(3f, 0f, 0f);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "platform")
            isCollided = false;
    }

    private void CheckInput()
    {
        var move = new Vector3(Input.GetAxisRaw("Horizontal"), 0);
        float movement = Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime;
        transform.position += move * moveSpeed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            GetComponent<SpriteRenderer>().flipX = true;
            pv.RPC("OnDirectionChange_LEFT", RpcTarget.Others);
        }
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            GetComponent<SpriteRenderer>().flipX = false;
            pv.RPC("OnDirectionChange_RIGHT", RpcTarget.Others);
        }

        if (movement != 0)
        {
            GetComponent<Animator>().SetBool("isWalking", true);
            pv.RPC("OnPlayerWalk", RpcTarget.Others);
        }
        else
        {
            GetComponent<Animator>().SetBool("isWalking", false);
            pv.RPC("OnPlayerStop", RpcTarget.Others);
        }
    }

    public void SendMessage(string message, string recipient)
    {
        Player[] players = PhotonNetwork.PlayerList;
        int[] rpcTargetPlayer = new int[1];
        chatBubble.SetActive(true);
        chatText.text = message;
        Invoke("ResetChat", 5f);

        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].NickName == recipient)
            {
                rpcTargetPlayer[0] = players[i].ActorNumber;
            }
        }

        object[] data = new object[] { message, photonView.ViewID };
        PhotonNetwork.RaiseEvent(CHAT_EVENT, data, new RaiseEventOptions { TargetActors = rpcTargetPlayer}, SendOptions.SendReliable);
        //pv.RPC("SendMessage_RPC", rpcTargetPlayer, message);
    }

    public void SendMessageGroup(string message, string recipient)
    {
        chatBubble.SetActive(true);
        chatText.text = message;
        Invoke("ResetChat", 5f);
        byte group = Byte.Parse(recipient);

        object[] data = new object[] { message, photonView.ViewID };
        PhotonNetwork.RaiseEvent(CHAT_EVENT, data, new RaiseEventOptions { InterestGroup = group }, SendOptions.SendReliable);
    }

    [PunRPC]
    void SendMessage_RPC(string message)
    {
        chatBubble.SetActive(true);
        chatText.text = message;
        Invoke("ResetChat", 5f);
    }

    [PunRPC]
    void OnPlayerWalk()
    {
        GetComponent<Animator>().SetBool("isWalking", true);
    }

    [PunRPC]
    void OnPlayerStop()
    {
        GetComponent<Animator>().SetBool("isWalking", false);
    }

    [PunRPC]
    void OnDirectionChange_LEFT()
    {
        GetComponent<SpriteRenderer>().flipX = true;
    }

    [PunRPC]
    void OnDirectionChange_RIGHT()
    {
        GetComponent<SpriteRenderer>().flipX = false;
    }

    private void ResetChat()
    {
        chatText.text = "";
        chatBubble.SetActive(false);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(transform.position);
        }
        else if(stream.IsReading)
        {
            smoothMove = (Vector3) stream.ReceiveNext();
        }
    }
}
