﻿using UnityEngine;
using Photon.Pun;

public class SimpleObjectMover : MonoBehaviourPun, IPunObservable
{
    public string color;
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        Vector3 position = new Vector3();
        Quaternion rotation = new Quaternion();

        if(stream.IsWriting){
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }else if(stream.IsReading){
            position = (Vector3) stream.ReceiveNext();
            rotation = (Quaternion) stream.ReceiveNext();
            Debug.Log("Slime "+color+" menerima info dari musuh" + position);
        }
    }
}
