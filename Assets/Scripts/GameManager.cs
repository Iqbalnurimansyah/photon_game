﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject[] playerPrefab;
    public GameObject gameCanvas;
    public GameObject sceneCamera;
    public GameObject chatPanel, groupPanel, chatGroupPanel;
    public TMP_InputField messageTF, usernameTF, messageGroupTF, groupTF;
    public Transform[] spawnPositions;

    private void Awake()
    {
        gameCanvas.SetActive(true);
    }

    public void SpawnPlayer(int idx)
    {
        int randomValue = Random.Range(0, 3);
        Vector3 spawnPos = new Vector3(spawnPositions[randomValue].position.x, spawnPositions[randomValue].position.y, spawnPositions[randomValue].position.z);
        PhotonNetwork.Instantiate(playerPrefab[idx].name, spawnPos, Quaternion.identity, 0);
        gameCanvas.SetActive(false);
        sceneCamera.SetActive(false);
    }

    public void OnChatClick()
    {
        chatPanel.SetActive(true); PlayerController[] players = FindObjectsOfType<PlayerController>();
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetIsMine())
            {
                players[i].enabled = false;
                break;
            }
        }
    }

    public void OnSendMessage()
    {
        string message = messageTF.text, recipient = usernameTF.text;
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        for(int i = 0; i < players.Length; i++)
        {
            if (players[i].GetIsMine())
            {
                players[i].enabled = true;
                players[i].SendMessage(message, recipient);
                break;
            }
        }
        chatPanel.SetActive(false);
    }

    public void OnClickJoinGroup()
    {
        groupPanel.SetActive(true);
    }

    public void OnJoinGroupClick(int group)
    {
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        for(int i = 0; i < players.Length; i++)
        {
            if (players[i].pv.IsMine)
            {
                players[i].SetGroup(group);
                break;
            }
        }
        groupPanel.SetActive(false);
    }

    public void OnChatGroupClick()
    {
        chatGroupPanel.SetActive(true); 
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetIsMine())
            {
                players[i].enabled = false;
                break;
            }
        }
    }

    public void OnSendGroupMessage()
    {
        string message = messageGroupTF.text, recipient = groupTF.text;
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetIsMine())
            {
                players[i].enabled = true;
                players[i].SendMessageGroup(message, recipient);
                break;
            }
        }
        chatGroupPanel.SetActive(false);
    }
}
